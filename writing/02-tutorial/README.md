# **Database Relationships: Understanding Cardinality and Participation**
## Why Relationships Matter and Why You Should Care. 
We store billions of bytes of data into databases every day. However, whenever we want to store or retrieve something from a database, we want to structure our data in a way that is meaningful and makes sense. In this tutorial, we will be working with relational databases. A relational database is a collection of data that are connected by pre-defined relationship. For example, let’s say we have a database that keeps track of departments, professors, and courses at a university. In a relational database context, Departments, Professors, and Courses are referred to as tables . Departments have many different professors and professors teach many courses so it is important that the tables gets linked together in our relational database.

One way that we can logically connect tables in a database is by using relationships. A relationship is simply a connection that exists between two logically-related tables. 

However, we can be more specific with the kind of relationship. It is important that you use the correct relationship in your database. If you use the correct relationship, your database will be much more efficient when you comes to storing data and pulling data from your database. If you do not use the correct relationships, you can end up with redundancy and duplicate information in your database. In this tutorial, you will learn about the different kind of relationship types and how you can implement relationships into your database or ER Model.

## Determining Cardinality
As you can imagine, there are many types of relations. One of the most important classifiers of a relation is its cardinality. Cardinality is simply the number of entities that relate to each other in a specific way. Although we may only have one manager, we could still have many co-workers. We need ways to represent different types of relations in order to model our data sets beter.

#### 2.1 Cardinality
There are three main cardinalities when we model relations in our data:

**1:1** - A relation between two singular entities. One employee has one office 

**1:N** - Many entities relate to one single entity in the same way. One manager manages many employees.

**N:M** - Similar to a 1:N relation, except the parent is not unique to a set of entities. One set of entities may relate similarly to one entity, but they could also relate to a different entity in the same way. A department may be working on several projects, but other departments also work on these projects.




#### 2.2 Representation of Relationships
Now how do we represent these types of relations in meaningful ways within our data?
In an Entity Relation diagram, we will have an object type (entity) and we need to show relations between entities. Relations are represented as diamonds labeled with the name of the relation. To show cardinality here we will first decide which cardinality best fits the relation between entities. Then we will link each entity to the relation and label the links with one side of the cardinality.

1 Employee has 1 office ->

![1 to 1 Relationship](images/oneToOne.png)

1 Manager manages N Employees ->   

![1 to N Relationship](images/OneToN.png)

N Departments work on M Projects ->

![N to M Relationship](images/NToM.png)

#### 2.3 Foreign Keys
Foreign Keys are what we use to connect entities together in relationships. We won't go too in depth into this topic, but it is useful to know. A foreign key is a unique identifier of one entity that is stored in another to allow us to eventually ‘join’ or relate our two pieces of data together.

Say we had an employee entity:
![First Table](images/CardFirstTable.png)

and Department:
![Second Table](images/CardSecondTable.png)

and we wanted to say that John work in testing and Alicia works in development we could represent that by adding a foreign key to our employee entity:
![Third Table](images/CardThirdTable.png)

This is how we would represent a basic 1:1 or 1:N relationship. If we want to represent an M:N relationship, we need to create a new entity that holds a foreign key of each entity it is relating. 


## Participation Constraints
There are two types of participation: total and partial. Let’s start with the formal definition, and then we can put it into more understandable terms after.

----
#### 3.1 Formal Definitions of Total and Partial Participation
The participation of an entity set, E, in a relationship set, R, is said to be total if every entity in E participates in at least one relationship. Partial Participation: The participation of an entity set, E, in a relationship set, R, is said to be partial if there exists an entity in E that does not participate in at least one relationship.
Do not worry if that does not make sense yet. We will break it down with an example.

#### 3.2 Example of Total Participation 
Suppose our entity set, E, is our three employees: Alice, Bob, and Charlie. Then let us suppose we have another entity set of departments: Engineering and Sales. 

![Initial Entities](images/firstTable.png)

Now let's say we have a relationship between our employees and department called WorksIn. That is to say, an employee works in a department. Let’s set up our relationship set as Alice works for Engineering, Bob works for Engineering, and Charlie works for Sales.

![WorksIn Relationship](images/secondTable.png)

In this example, every employee is assigned to a department. Therefore we say employees have total participation in the WorksIn relationship.

#### 3.3 Example of Partial Participation
Now for a partial participation example. Continuing with the previous example, suppose we add another entity to the employee set, Dave, but we do not assign Dave to a department.

![Partial Participation](images/firstTable.png)

Now employees are said to have partial participation in its relationship with departments because not every employee works in a department.
If you have noticed, we have been talking about employee’s participation in the relationship, but not department. However, it is possible to talk about the participation of both entities in a relationship. If you look back at the last example, you notice every department is in the WorksIn relationship, so it has total participation. Thus we can say that in the WorksIn relationship, employee has partial participation while department has full participation.

#### 3.4 Representing Participation in an ER Diagram
Suppose we have the entity sets of Students and Teams and relationship sets of MemberOf and LeaderOf.

![Participation Diagram](images/ParticipationER.png)
Image from [Participation-and-Cardinality-Constraints](https://www.slideshare.net/NikhiDeswal/cardinality-and-participation-constraints)

Every student must be a member of a team, so it has total participation. As you can see, this is represented by a double line. A student can be a leader of a team, so it only has a partial participation with the LeaderOf relationship. That is represented by a single line.

**Pop quiz**: what is team’s participation in the MemberOf relationship?

The answer is partial participation. You can tell this because of the single line connecting team and MemberOf.

## Practice Problems
Now let's practice what we have learned by looking at some relationships from a local university. 

For each of the following make the relationship as 1:1, 1:N, N:1 or M:N. If you are unsure or the answer is unclear think about what questions you could ask to clarify. Scroll to the bottom to check your answers!

1. Student : SSN
2. Faculty : Department
3. Grad Student : Advisor
4. Department : College
5. Student : Course
6. Professor : Coffee Mug

For each fo the following, think of which level of participation, total or partial, makes the most sense for the entities. Some of these have multiple correct answers based on your assumptions, so think through your assumptions before answering:

1. Employee hasA SSN
2. Employee worksIn Office
3. Employee worksFor Department
4. Employee manages Department

The solutions are at the bottom once you finish.

Now try to generate some examples of the different types of cardinality constraints on your own. Give an explanation for why the relationship has the given ratio. Indicate if there is a total or partial participation constraint.
Give an example of a 1:1 relationship.
Give an example of a 1:N relationship.
Give an example of a M:N relationship.

## References and Extra Resources
Fundamentals of Database Systems by Elmasri and Navathe, pages 88-95 are helpful with understanding participation and cardinality.

[Wofford--ecs.org/DataAndVisualiaztion](https://wofford-ecs.org/DataAndVisualization/ermodel/material.htm) provides a clear examples of entity-relationships with examples of cardinality constraints and practice problems.

[Cardinality Ratio](https://www.youtube.com/watch?v=GMB2ySkx5y8) is a brief video that gives a nice description of cardinality ratios.

If you want to learn more about representing relations using foreign keys check out this link:
https://dzone.com/articles/how-to-handle-a-many-to-many-relationship-in-datab

Check out https://www.slideshare.net/NikhilDeswal/cardinality-and-participation-constraints for more help on both cardinaility and participation.

## Practice Problem Solutions
Cardility Questions:
1. Student : SSN is a 1:1 relationship. For each student there is 1 SSN and for each SSN there is a unique student.
2. A good question to ask yourself is: “Can a faculty belong to multiple departments?” Assuming yes faculty : department is a M:N relationship. Each faculty member can belong to multiple departments and each department has multiple faculty member
3. Grad Student : Advisor is a N:1 relationship. Each grad student has one advisor but each advisor can have multiple grad students.
4. Department : College is a N:1 relationship. Each department is in one college but each college has multiple departments.
5. Student : Course is a M:N relationship. Each student takes multiple courses and each course has multiple students.
6. A good question to ask yourself is: “Can a professor have more than one coffee mug?” and “Do professors share mugs?” Assuming no to both, professor : coffee mug is a 1:1 relationship. If a professor can have one coffee mug then it is a N:1 relationship. If yes to both, then it is an N:M relationship.

Participation Questions:
1. Employee and SSN will both have total participation since they cannot exist without each other.
2. Employee and Office will both have partial participation. This is assuming that an employee does not have to have an office to work, and a company can have unused offices. If you had different assumptions, you could be right as well.
3. Employee and Department will both have full participation. This is assuming a department must have an employee, and an employee must have a department to work for. If you had different assumptions, you could be right as well.
4. Employee will have partial participation and department will have total participation. This is assuming that department must have a manager. If you assumed it was not necessary, then department will have partial participation.