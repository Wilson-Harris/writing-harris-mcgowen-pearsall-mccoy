# Team Members

* Wilson Harris
* Justin McGowen
* Reese Pearsall
* Brad McCoy

# Idea 1: Drawing an Entity for an ER Diagram
The objective of this course would be to teach someone how to draw an entity and its attributes in an ER diagram.
We would teach them about atomic, composite, dervied, multivalue, and key attributes. 
The reason someone would take this course it to better understand how to model data for more understanding of it.
Also it would help them begin to understand setting up a database.

# Idea 2: Relational Algebra
The objective of this course would be to teach fundamentals of relational algebra. We would teach select, project,
renames, and joins.
The reason someone would take this course to build a foundation for how to use SQL and databases.

# Idea 3: Relationship Types in Schemas
The objective of this course would be to teach someone how to represent different relationships in a schema. We
would 1:1, 1:N, and N:M relationships as well as how to use foreign keys.
The reason someone would take this course is to understand how entities are connected in a database.
